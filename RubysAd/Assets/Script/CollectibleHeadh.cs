﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleHeadh : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        RubyController ruby = collision.GetComponent<RubyController>();
        if (ruby != null)
        {
            ruby.ChangeHealth(1);
            Destroy(gameObject);
        }
    }
}
