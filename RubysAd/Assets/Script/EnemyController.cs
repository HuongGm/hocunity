﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    Rigidbody2D rb;
    public float speed;
    public bool vertical;
    public float ChangeTimer = 2f;
    float timer;
    int direction = 1;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        timer = ChangeTimer;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            direction = -direction;
            timer = ChangeTimer;
        } 
    }
    private void FixedUpdate()
    {
        Vector2 position = rb.position;
        if (vertical)
        {
            position.y = position.y + speed * Time.deltaTime * direction;
        }
        else
        {
            position.x = position.x + speed * Time.deltaTime * direction;
        }
        rb.MovePosition(position);
    }

}
