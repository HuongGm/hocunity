﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RubyController : MonoBehaviour
{
    Rigidbody2D rb;

    float horizontal;
    float vertical;
    public float speed;

    public GameObject projectile;
    Vector2 lookDirection = new Vector2(1, 0);

    public float timeInvincible = 2f;
    bool isInvincible;
    float currenttime;

    public int maxhealth = 10;
    int currentHealth;
    // Start is called before the first frame update
    void Start()
    {
        
        rb = GetComponent<Rigidbody2D>();
        currentHealth = maxhealth;
    }

    // Update is called once per frame
    void Update()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");

        Vector2 move = new Vector2(horizontal, vertical);

        if (!Mathf.Approximately(move.x, 0.0f) || !Mathf.Approximately(move.y, 0.0f))
        {
            lookDirection.Set(move.x, move.y);
            lookDirection.Normalize();
        }

        if (isInvincible)
        {
            currenttime -= Time.deltaTime;
            if (currenttime < 0)
            {
                isInvincible = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            Launch();
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            RaycastHit2D hit = Physics2D.Raycast(rb.position + Vector2.up * 0.2f, lookDirection, 1.5f, LayerMask.GetMask("NPC"));
            
            if (hit.collider != null)
            {
                Debug.Log("vao day");
                ShowBox character = hit.collider.GetComponent<ShowBox>();
                if (character != null)
                {
                    character.DisplayDialog();
                }
            }
        }
    }
    private void FixedUpdate()
    {
        Vector2 position = rb.position;
        position.x = position.x + speed * horizontal * Time.deltaTime;
        position.y = position.y + speed * vertical * Time.deltaTime;
        rb.MovePosition(position);
    }
    void Launch()
    {
        Debug.Log("hi");
        GameObject projectileOb = Instantiate(projectile, rb.position + Vector2.up, Quaternion.identity);
        Projectile projectileCom = projectileOb.GetComponent<Projectile>();
        projectileCom.Launch(lookDirection, 400);
    }
    public void ChangeHealth(int health)
    {
        if (health < 0)
        {
            if (isInvincible)
                return;

            isInvincible = true;
            currenttime = timeInvincible;
            Debug.Log($"currenttime: {currenttime}");

        }

        Debug.Log($"Truoc==currentHealth: {currentHealth}");

        currentHealth = Mathf.Clamp(currentHealth + health, 0, maxhealth);

        Debug.Log($"sau==currentHealth: {currentHealth}");
    }
}
