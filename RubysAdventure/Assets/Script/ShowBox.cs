﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowBox : MonoBehaviour
{
    // hiên Dialog được gắn vào Jambe và ân đi với thời gian xác định
    public float displayTime = 4.0f;
    public GameObject dialogBox;// canvas dialog box
    float timerDisplay;// lưu thời gian của bộ đếm thời gian

    void Start() // đặt trạng thái ban đầu của Dialogbox là ẩn
    {
        dialogBox.SetActive(false);
        timerDisplay = -1.0f;
    }

    void Update()
    {
        if (timerDisplay >= 0)
        {
            timerDisplay -= Time.deltaTime;
            if (timerDisplay < 0)
            {
                dialogBox.SetActive(false);
            }
        }
    }

    public void DisplayDialog()
    {
        timerDisplay = displayTime;
        dialogBox.SetActive(true);
    }
}
