﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthCollectible : MonoBehaviour
{
    
    public AudioClip collectedClip;
    void OnTriggerEnter2D(Collider2D other)// cộng thêm máu cho Player khi chạm vào item Health
    {
        RubyController controller = other.GetComponent<RubyController>();

        if (controller != null)// kiểm tra có va chạm với object Ruby ko 
        {
            if (controller.health < controller.maxHealth) // kiểm tra máu hiện tại có nhỏ hơn máu tối đa ko
            {
                controller.ChangeHealth(1);
                Destroy(gameObject);
                controller.PlaySound(collectedClip);
            }
        }
    }
}
