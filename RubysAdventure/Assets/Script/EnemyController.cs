﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    // làm chuyển động cho enemy sẽ tự động quay trái, phải hoặc lên xuống với thời gian xác đinh, sẽ bị dừng chuyển động khi đặt biến broken thành true
    public float speed;
    public bool vertical;
    public float changeTime = 3.0f;
    public ParticleSystem smokeEffect;

    Rigidbody2D rigidbody2D;
    Animator animator;
    float timer;
    int direction = 1;

    bool broken = false;


    
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        timer = changeTime;
    }

    void Update()
    { //
        if (broken)
        {
            return;
        }
        // giảm dần giá trị của timer theo thời gian
        timer -= Time.deltaTime;

        if (timer < 0)
        {
            direction = -direction;
            timer = changeTime;
        }
    }

    void FixedUpdate()
    { // 
        if (broken)
        {
            return;
        }
        Vector2 position = rigidbody2D.position;

        if (vertical)
        {
            position.y = position.y + Time.deltaTime * speed * direction;
            animator.SetFloat("MoveX", 0);              // truyền vào animator giá trị parameter
            animator.SetFloat("MoveY", direction);
        }
        else
        {
            position.x = position.x + Time.deltaTime * speed * direction;
            animator.SetFloat("MoveX", direction);
            animator.SetFloat("MoveY", 0);
        }

        rigidbody2D.MovePosition(position);
    }
    void OnCollisionEnter2D(Collision2D other)// được gọi khi chạm vào collision bất kì
    {
        RubyController player = other.gameObject.GetComponent<RubyController>();// kiểm tra GameObject va phải có phải là tập lệnh RubyController rồi trừ máu của Ruby

        if (player != null)
        {
            player.ChangeHealth(-1);
        }
    }
    public void Fix() // được gọi khi enemy trạm projectile sẽ dừng hoạt động của enemy
    {
        broken = true;
        rigidbody2D.simulated = false;// Loại bỏ Rigibody khỏi mô phỏng vật lý,
        animator.SetTrigger("Fixed");
        smokeEffect.Stop();
    }
}
