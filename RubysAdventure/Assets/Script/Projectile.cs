﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    Rigidbody2D rigidbody2d;
   
    void Awake()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    { // kiểm tra nếu có trên 1000 Object Projectile sẽ destroy GameObject
        if (transform.position.magnitude > 1000.0f)
        {
            Destroy(gameObject);
        } 
    }
    public void Launch(Vector2 direction, float force) // app lực cho Projectile
    {
        rigidbody2d.AddForce(direction * force);
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        // kiểm tra project có chạm vào Enemy ko rồi gọi vào hàm fix bên tập lệnh của Enemy
        EnemyController e = other.collider.GetComponent<EnemyController>();
        if (e != null)
        {
            e.Fix();
        }
        Destroy(gameObject);
    }
}
