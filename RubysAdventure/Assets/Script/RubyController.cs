﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RubyController : MonoBehaviour
{
    public float speed = 3.0f;// tốc độ đặt cho player
    public int maxHealth = 5;// máu tối đa của Player
    public int health { get { return currentHealth; } }// tạo biến để bên ngoài lấy được máu hiện tại của phayer 
    int currentHealth;// máu player hiện tại

    public float timeInvincible = 2.0f;
    bool isInvincible;// lưu trạng thái hiện tại Ruby có bị trừ máu hay không
    float invincibleTimer;// lưu trữ khoảng thời gian còn lại mà Ruby ko bị trừ máu

    Rigidbody2D rigidbody2d;
    float horizontal;
    float vertical;
    Animator animator;
    Vector2 lookDirection = new Vector2(1, 0);

    public GameObject projectilePrefab;
    AudioSource audioSource;
    public AudioClip hit;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        currentHealth = maxHealth;
    }

    public void PlaySound(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }
    // Update is called once per frame
    void Update()
    {

         horizontal = Input.GetAxis("Horizontal");// lấy giá trị trục nằm ngang
         vertical = Input.GetAxis("Vertical");// lấy giá trị trục dọc

        Vector2 move = new Vector2(horizontal, vertical);

        if (!Mathf.Approximately(move.x, 0.0f) || !Mathf.Approximately(move.y, 0.0f))
        {
            lookDirection.Set(move.x, move.y);
            lookDirection.Normalize();
        }

        animator.SetFloat("LookX", lookDirection.x);
        animator.SetFloat("LookY", lookDirection.y);
        animator.SetFloat("Speed", move.magnitude);

        if (isInvincible)
        {
            invincibleTimer -= Time.deltaTime; // đếm ngược thời gian
            if (invincibleTimer < 0)
                isInvincible = false;
            animator.SetTrigger("Hit");
        }
        if (Input.GetKeyDown(KeyCode.C))// gọi tới hàm Launch khi nhấn phím C
        {
            PlaySound(hit);
            Launch();
        }
        if (Input.GetKeyDown(KeyCode.X))// khi nhấn phím X sẽ dùng Raycast quét có collider trước mặt với hướng, khoảng cách và lớp được xác định
        {
            RaycastHit2D hit = Physics2D.Raycast(rigidbody2d.position + Vector2.up * 0.2f, lookDirection, 1.5f, LayerMask.GetMask("NPC"));
            if (hit.collider != null)
            {
                ShowBox character = hit.collider.GetComponent<ShowBox>();
                if (character != null)
                {
                    character.DisplayDialog();
                }
            }
        }
    }
    void FixedUpdate() 
    {
        Vector2 position = transform.position;// di chuyển Player
        position.x = position.x + speed * horizontal * Time.deltaTime;
        position.y = position.y + speed * vertical * Time.deltaTime;
        //Debug.Log(position);
        rigidbody2d.MovePosition(position);
    }

    public void ChangeHealth(int amount)
    {
        if (amount < 0)// nếu Ruby bị trừ máu
        {
            if (isInvincible)//Kiểm tra Ruby có bất tử chưa
                return;

            isInvincible = true;
            invincibleTimer = timeInvincible;
        }
        currentHealth = Mathf.Clamp(currentHealth + amount, 0, maxHealth);// so sánh máu hiện tại nếu nhỏ hơn 0 sẽ bằng ko , nếu lớn hơn máu tối đa sẽ bằng máu tối đa
        UIHealthBar.instance.SetValue(currentHealth / (float)maxHealth);// gọi tới hàm setvalue của script UIHealthBar
    }
    void Launch() // tạo ra Projectile tại vị trí player
    {
        GameObject projectileObject = Instantiate(projectilePrefab, rigidbody2d.position + Vector2.up * 0.5f, Quaternion.identity);

        Projectile projectile = projectileObject.GetComponent<Projectile>();
        projectile.Launch(lookDirection, 300);

        animator.SetTrigger("Launch");
    }
}
