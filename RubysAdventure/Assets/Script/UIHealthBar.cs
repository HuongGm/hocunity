﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHealthBar : MonoBehaviour
{
    // set width cho HealthBar ở ui
    public static UIHealthBar instance { get; private set; }

    public Image mask;
    float originalSize;// kích thước ban đầu của mask

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        originalSize = mask.rectTransform.rect.width;
    }

    public void SetValue(float value)
    {
        mask.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, originalSize * value);
        //SetSizeWithCurrentAnchors( Trục để chỉ định kích thước,Kích thước mong muốn dọc theo trục được chỉ định)
    }
}
